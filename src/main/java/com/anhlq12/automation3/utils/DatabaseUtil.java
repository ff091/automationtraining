package com.anhlq12.automation3.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseUtil {

	public static Connection getConnection(String dbURL, String userName, String password) {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(dbURL, userName, password);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return conn;
	}

	public static ResultSet fetch(Connection conn, String query) {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if (stmt.execute(query)) {
				rs = stmt.getResultSet();
			}
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return rs;
	}
}
