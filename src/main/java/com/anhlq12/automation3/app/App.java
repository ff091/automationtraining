package com.anhlq12.automation3.app;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.anhlq12.automation3.entities.Account;
import com.anhlq12.automation3.models.AccountModel;
import com.anhlq12.automation3.utils.DatabaseUtil;
import com.anhlq12.automation3.utils.Utilities;

public class App {
	
	private static String server = Utilities.getProperty("server");
	private static String hostname = Utilities.getProperty("hostname");
	private static String port = Utilities.getProperty("port");
	private static String username = Utilities.getProperty("username");
	private static String password = Utilities.getProperty("password");
	private static String databaseName = Utilities.getProperty("databaseName");
	
	public static void main(String[] args) {
		
		String dbURL = "jdbc:"+server+"://"+hostname+":"+port+";"
	            + "databaseName="+databaseName +";";
		Connection conn = DatabaseUtil.getConnection(dbURL, username, password);
		AccountModel mAccount = new AccountModel(conn);
		Scanner input = new Scanner(System.in);
		boolean cont = true;
		while (cont) {
			inMenu();
			int nhap = luaChonNhap(input);
			switch (nhap) {
			case 1:
				mAccount.printOut(mAccount.getAll());
				break;
			case 2:
				int id = nhapID(input);
				Account account = mAccount.getByAccountID(id);
				if(account != null) {
					System.out.println(account);
					continue;
				}
				System.out.println("Không tồn tại tài khoản");
				break;
			case 3:
				String kwd = nhapTuKhoa(input);
				mAccount.printOut(mAccount.getByKeywords(kwd));
				break;
			case 4:
				String[] info = nhapTTTK(input);
				account = new Account(info[0], info[1]);
				boolean isAdd = mAccount.insertInto(account);
				if(isAdd) {
					System.out.println("Thêm mới tài khoản thành công");
					continue;
				}
				System.out.println("Thêm mới tài khoản thất bại");
				break;
			case 5:
				id = nhapID(input);
				boolean isDel = mAccount.deleteFrom(id);
				if(isDel) {
					System.out.println("Xóa tài khoản thành công");
					continue;
				}
				System.out.println("Xóa tài khoản thất bại");
				break;
			case 6:
				List<String> ls = mAccount.getCSVFormat(mAccount.getAll());
				String[] headers = {"ID", "ACCOUNTID", "FULLNAME", "ADDRESS"}; 
				boolean isImport = Utilities.writeCSV("accounts.csv", headers, ls);
				if(isImport) {
					System.out.println("Import thành công");
					continue;
				}
				System.out.println("Import không thành công");
				break;
			case 7:
				List<String[]> list = Utilities.readCSV("accounts.csv");
				List<Account> accounts = new ArrayList<Account>();
				for(String[] object : list) {
					int id1 = Integer.parseInt(object[0]);
					int accountId1 = Integer.parseInt(object[1]);
					String fullname1 = object[2];
					String address1 = object[3];
					accounts.add(new Account(id1, accountId1, fullname1, address1));
				}
				mAccount.printOut(accounts);
				break;
			case 8:
				String thoat = luaChonThoat(input);
				if ("không".equals(thoat)) {
					continue;
				}
				System.out.println("Thoát chương trình");
				System.exit(0);
			}
		}
	}

	public static int luaChonNhap(Scanner input) {
		System.out.println("Nhập lựa chọn của bạn: ");
		int opt = -1;
		while (true) {
			try {	
				opt = input.nextInt();
//				input.nextLine();
				if (opt == 1 || opt == 2 || opt == 3 || opt == 4 || opt == 5 || opt == 6 || opt == 7 || opt == 8) {
					return opt;
				}
			} catch (InputMismatchException e) {
				
			} finally {
				input.nextLine();
			}
			System.out.println("Lựa chọn không hợp lệ. Vui lòng nhập lại: ");
		}
	}

	public static void inMenu() {
		System.out.println("1. Hiển thị thông tin của tất cả tài khoản");
		System.out.println("2. Hiển thị thông tin của account dựa vào AccountID");
		System.out.println("3. Tìm kiếm account theo từ khóa (AccountID, hoặc Fullname)");
		System.out.println("4. Thêm mới tài khoản");
		System.out.println("5. Xóa tài khoản theo AccountID");
		System.out.println("6. Import thông tin tài khoản (CSV)");
		System.out.println("7. Export thông tin tài khoản (CSV)");
		System.out.println("8. Thoát");
	}

	public static String luaChonThoat(Scanner input) {
		System.out.println("Bạn có chắc chắn muốn thoát chương trình không?");
		String opt = null;
		while (true) {
			opt = input.nextLine().trim();
			if ("có".equalsIgnoreCase(opt) || "không".equalsIgnoreCase(opt)) {
				return opt;
			}
			System.out.println("Lựa chọn không hợp lệ. Vui lòng nhập lại: ");
		}
	}
	
	public static int nhapID(Scanner input) {
		System.out.println("Nhập AccountID: ");
		int id = -1;
		while (true) {
			try {	
				id = input.nextInt();
				return id;
			} catch (InputMismatchException e) {

			} finally {
				input.nextLine();
			}
			System.out.println("Lựa chọn không hợp lệ. Vui lòng nhập lại: ");
		}
	}
	
	public static String nhapTuKhoa(Scanner input) {
		System.out.println("Nhập từ khóa: ");
		return input.nextLine();
	}
	
	public static String[] nhapTTTK(Scanner input) {
		String[] info = new String[2];
		System.out.println("Nhập thông tin tài khoản [fullname]: ");
		System.out.println("Nhập tên đầy đủ: ");
		String fullname = input.nextLine();
		info[0] = fullname;
		System.out.println("Nhập địa chỉ [address]: ");
		String address = input.nextLine();
		info[1] = address;
		return info;
	}
}
